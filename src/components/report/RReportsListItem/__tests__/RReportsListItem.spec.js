import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import RReportsListItem from '../RReportsListItem';

expect.extend(toHaveNoViolations);

const propsData = {
  report: [],
  timeIndex: 1,
};

describe('RReportsListItem', () => {
  test('accessibility', async () => {
    const wrapper = mount(RReportsListItem, {
      propsData,
    });

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(RReportsListItem, {
      propsData,
    });

    expect(wrapper.vm).toBeTruthy();
  });

  test('getTime computed property', () => {
    const wrapper = mount(RReportsListItem, {
      propsData,
    });

    expect(wrapper.vm.getTime()).toEqual('01:00');
    expect(wrapper.vm.getTime(1)).toEqual('02:00');
  });
});
