import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { toHaveNoViolations } from 'jest-axe';
import report from '@/store/modules/report';

import RReportsList from '../RReportsList';

expect.extend(toHaveNoViolations);

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    report,
  },
});

describe('XAttributeGroupsListItem', () => {
  test('accessibility', async () => {
    const wrapper = shallowMount(RReportsList, {
      store,
      localVue,
    });

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = shallowMount(RReportsList, {
      store,
      localVue,
    });

    expect(wrapper.vm).toBeTruthy();
  });
});
