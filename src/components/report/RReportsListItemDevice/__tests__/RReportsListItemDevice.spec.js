import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import RReportsListItemDevice from '../RReportsListItemDevice';

expect.extend(toHaveNoViolations);

const propsData = {
  device: {},
};

describe('RReportsListItemDevice', () => {
  test('accessibility', async () => {
    const wrapper = mount(RReportsListItemDevice, {
      propsData,
    });

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(RReportsListItemDevice, {
      propsData,
    });

    expect(wrapper.vm).toBeTruthy();
  });
});
