import { mount } from '@vue/test-utils';
import { toHaveNoViolations } from 'jest-axe';

import RProgress from '../RProgress';

expect.extend(toHaveNoViolations);

describe('RProgress', () => {
  it('accessebility', async () => {
    const wrapper = mount(RProgress);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  it('remove isFullPage property', async () => {
    const wrapper = mount(RProgress, {
      propsData: {
        isFullPage: false,
      },
    });

    expect(!wrapper.classes().includes('is-full-page')).toBeTruthy();
  });
});
