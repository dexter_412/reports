import { mount } from '@vue/test-utils';
import { axe, toHaveNoViolations } from 'jest-axe';

import RCol from '../RCol';

expect.extend(toHaveNoViolations);

describe('RCol', () => {
  test('accessebility', async () => {
    const wrapper = mount(RCol);

    const results = await axe(wrapper.element);
    expect(results).toHaveNoViolations();
  });

  test('is a Vue instance', () => {
    const wrapper = mount(RCol);

    expect(wrapper.vm).toBeTruthy();
  });

  test('slot text', () => {
    const wrapper = mount(RCol, {
      slots: {
        default: 'test',
      },
    });
    expect(wrapper.text()).toBe('test');
  });

  test('span prop', () => {
    const wrapper = mount(RCol, {
      propsData: {
        span: 1,
      },
      slots: {
        default: 'test',
      },
    });
    expect(wrapper.classes()).toContain('r-col_1');
  });

  test('offset prop', () => {
    const wrapper = mount(RCol, {
      propsData: {
        offset: 1,
      },
      slots: {
        default: 'test',
      },
    });
    expect(wrapper.classes()).toContain('r-col_offset_1');
  });

  test('push prop', () => {
    const wrapper = mount(RCol, {
      propsData: {
        push: 1,
      },
      slots: {
        default: 'test',
      },
    });
    expect(wrapper.classes()).toContain('r-col_push_1');
  });

  test('pull prop', () => {
    const wrapper = mount(RCol, {
      propsData: {
        pull: 1,
      },
      slots: {
        default: 'test',
      },
    });
    expect(wrapper.classes()).toContain('r-col_pull_1');
  });
});
