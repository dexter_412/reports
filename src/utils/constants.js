// Base constants
export const BASE_URL = 'http://localhost:3000/';

// Api urls
export const GET_REPORT_URL = 'report';

// Date formats
export const FORMAT_OF_DATE = 'MM/dd/yyyy';
