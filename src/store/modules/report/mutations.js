/**
 * @module store/report/mutations
 *
 * @version 1.0.0
 * @copyright viax.io 2020
 */

import * as types from './mutation-types';

export default {
  /**
   * Mutation: Set report
   *
   * @param {object} state - vuex state
   * @param {array} report - report
   *
   * @since Version 1.0.0
   */

  [types.SET_REPORT](state, report) {
    state.report = report;
    state.isLoadingReport = false;
  },

  /**
   * Mutation: Start load report
   *
   * @param {object} state - vuex state
   *
   * @since Version 1.0.0
   */

  [types.START_LOAD_REPORT](state) {
    state.isLoadingReport = true;
  },
};
