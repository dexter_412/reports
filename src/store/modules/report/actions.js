/**
 * @module store/report/actions
 *
 * @version 1.0.0
 * @copyright reports 2020
 */

import  Axios from 'axios';
import {
  BASE_URL,
  GET_REPORT_URL,
} from '@/utils';
import * as types from './mutation-types';

/**
 * Action: Get report
 *
 * @since Version 1.0.0
 */
const getReport = async ({ commit }) => {
  commit(types.START_LOAD_REPORT);
  Axios.get(`${BASE_URL}${GET_REPORT_URL}`)
    .then(({ data }) => {
      setTimeout(() => {
        commit(types.SET_REPORT, data);
      }, 1000);
    });
};

export default {
  getReport,
};
