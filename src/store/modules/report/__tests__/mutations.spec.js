import * as types from '../mutation-types';
import mutations from '../mutations';

describe('Reports store: Mutations', () => {
  test('SET_REPORTS', () => {
    const state = {
      reports: [],
      isLoadingReport: true,
    };

    mutations[types.SET_REPORT](state, [
      {
        time: '00:00 - 01:00',
        devices: [
          {
            location: 'Greenhouse',
            device: 'Humidity',
            min: 80,
            max: 100,
            avg: 85,
            median: 80,
          },
        ],
      },
    ]);
    expect(state.isLoadingReport).toBeFalsy();
    expect(state.report).toEqual([
      {
        time: '00:00 - 01:00',
        devices: [
          {
            location: 'Greenhouse',
            device: 'Humidity',
            min: 80,
            max: 100,
            avg: 85,
            median: 80,
          },
        ],
      },
    ]);
  });

  test('START_LOAD_REPORTS', () => {
    const state = {
      isLoadingReports: false,
    };

    mutations[types.START_LOAD_REPORT](state);
    expect(state.isLoadingReport).toBeTruthy();
  });
});
