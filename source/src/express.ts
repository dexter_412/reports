import * as express from 'express';
import * as cors from 'cors';
import { generateResults } from './functionality';

const app = express();

app.use(cors());
app.get('/report', async (req, res) => {
  res.json(await generateResults());
});

app.listen(3000, () => console.log('Server listening on port 3000'));
