import {generateResults, writeResultsToFile} from './functionality';

async function main(): Promise<void> {
  const {date, results} = await generateResults();
  await writeResultsToFile(results, date);
}

main();
