const fs = require('fs').promises;

const resultsFilePath = 'assets/results.txt';
const devicesFilePath = 'assets/devices.csv';
const outputFilePath = 'assets/output.txt';

class DeviceOutput {
  timestamp: Date;
  deviceId: string;
  value: number;

  constructor(raw: string) {
    if (raw.charAt(0) === '{') {
      this.parseJSON(raw);
    } else {
      this.parseTSV(raw);
    }
  }

  private parseJSON(raw: string): void {
    const parsed: {ts: string, id: string, value: string} = JSON.parse(raw);
    this.timestamp = new Date(parsed.ts);
    this.value = Number.parseFloat(parsed.value);
    this.deviceId = parsed.id;
  }

  private parseTSV(raw: string): void {
    const parsed = raw.split('\t');
    this.deviceId = parsed[0];
    this.timestamp = new Date(Number.parseInt(parsed[1]));
    this.value = Number.parseFloat(parsed[2]);
  }
}

class Device {
  id: string;
  name: string;
  location: string;

  constructor(raw: string) {
    const parsed = raw.split(',');
    this.id = parsed[0];
    this.name = parsed[1];
    this.location = parsed[2];
  }
}

export class DeviceResult {
  name: string;
  location: string;
  min: number;
  max: number;
  avg: number;
  median: number;

  constructor(device: Device, outputs: DeviceOutput[]) {
    this.name = device.name;
    this.location = device.location;

    this.max = Math.max.apply(this, outputs.map(out => out.value));
    this.min = Math.min.apply(this, outputs.map(out => out.value));

    const outputsLen = outputs.length;
    this.avg = outputs.reduce((sum, el) => sum + el.value, 0) / outputsLen;

    const outputsMidIndex = ~~(outputsLen / 2);
    const values = outputs.map(output => output.value).sort();
    this.median = outputsLen % 2 === 0 ? (values[outputsMidIndex] + values[outputsMidIndex - 1]) / 2 : values[outputsMidIndex];
  }

  toString(): string {
    return `| ${this.location}: ${this.name}\t| ${this.min.toFixed(0)}\t| ${this.max.toFixed(0)}\t| ${this.avg.toFixed(0)}\t| ${this.median.toFixed(0)}\t|`;
  }
}

async function readDataFromFile(path: string): Promise<string[]> {
  const file = await fs.readFile(path);
  return file
    .toString()
    .split('\r\n');
}

export async function writeResultsToFile(results: Array<DeviceResult[]>, date: Date): Promise<void> {
  const formatHour = (hour: number) => `${hour > 9 ? hour : '0' + hour}:00`;

  const resultStr = results.reduce((str: string, deviceResults: DeviceResult[], i: number) => {
    const [, month, day, year] = date.toDateString().split(' ');
    const dateStr = `${day}-${month}-${year} ${formatHour(i)}-${formatHour(i + 1)}\n`;
    const descrStr = '| Device\t| Min\t| Max\t| Avg\t| Median\t|\n';
    return str + dateStr + descrStr + deviceResults.map(res => res.toString()).join('\n') + '\n\n';
  }, '');
  await fs.writeFile(outputFilePath, resultStr);
}

function getDevicesResults(devices: Device[], devicesOutputs: DeviceOutput[]): Array<DeviceResult[]> {
  const results = [];
  const dayHours = 24;
  for (let i = 0; i < dayHours; i++) {
    const hourOutputs = devicesOutputs.filter(output => output.timestamp.getHours() === i);
    if (!hourOutputs.length) continue;

    const devicesResults = devices
      .map(device => {
        const devicePeriodOutputs = hourOutputs.filter(out => device.id === out.deviceId);
        return devicePeriodOutputs.length ? new DeviceResult(device, devicePeriodOutputs) : null;
      })
      .filter(result => !!result);
    results.push(devicesResults);
  }
  return results;
}

export async function generateResults(): Promise<{date: Date, results: Array<DeviceResult[]>}> {
  const devicesOutputsRaw = await readDataFromFile(resultsFilePath);
  const devicesOutputs = devicesOutputsRaw.map(deviceOutputRaw => new DeviceOutput(deviceOutputRaw));

  const devicesRaw = await readDataFromFile(devicesFilePath);
  const devices = devicesRaw.map(deviceRaw => new Device(deviceRaw));

  return {date: devicesOutputs[0].timestamp, results: getDevicesResults(devices, devicesOutputs)};
}
