import { format } from 'date-fns';

describe('Reports View', () => {
  it('check reports list displaying', () => {
    cy.visit('');
    cy.contains('[data-test=date-info]', `Report for: ${format(new Date(), 'MM/dd/yyyy')}`);
  });

  it('check device data', () => {
    cy.visit('');
    cy.get('[data-test=device-name]').should('exist');
    cy.get('[data-test=device-min]').should('exist');
    cy.get('[data-test=device-max]').should('exist');
    cy.get('[data-test=device-avg]').should('exist');
    cy.get('[data-test=device-median]').should('exist');
  });
});
